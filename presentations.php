<?php
$page = 'presentations';
include ('sections/header.php');
?>
<main class='text-center'>
	<h2>Presentations</h2>	
	<div class='space'></div>

	<?php
	chdir('media/presentations');
	$dirs = array_diff(glob('*', GLOB_ONLYDIR), array('.', '..'));
	foreach ($dirs as $dir): if (!is_dir($dir)) continue; ?>
	
	<div class='presentation'>
		<h3><?php echo $dir; ?></h3>
		<div class='description'>
			<?php echo @file_get_contents("$dir/desc.txt"); ?>
		</div>
		<div class='downloads'>
			<?php echo "<a href='media/presentations/$dir/$dir.pptx' alt='$dir - Powerpoint'>PPTX</a>"; ?>
			<?php echo "<a href='media/presentations/$dir/$dir.pdf' alt='$dir - Adobe PDF'>PDF</a>"; ?>
		</div>
	</div>
	
	<?php endforeach; ?>
</main>
<?php include ('sections/footer.php') ?>