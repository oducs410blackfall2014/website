<?php
$page = 'probsol';
include ('sections/header.php');
?>
<main class='text-center'>
	<h2>The Societal Problem</h2>
	Many endangered/extinct languages do not have a written alphabet which poses a problem for their preservation.
	
	<div class='space'></div>
	
	<h2>Our Solution</h2>
  <p>
	Our goal is to create a standalone product to assist in the creation of a written syllabary. To accomplish this
  Sequoyah provides the following features:
  <ul>
    <li>Users can assign custom symbols to <a class='link' href='http://en.wikipedia.org/wiki/Phoneme'>phonemes</a></li>
    <li>Phoneme symbols can be combined to generate syllabary symbols</li>
    <li>Full customization of syllabary entries, including symbol editing</li>
    <li><a class='link' href='http://en.wikipedia.org/wiki/TrueType'>TrueType</a> font file generation for writing digital documents in the language</li>
  </ul>
  </p>
</main>
<?php include ('sections/footer.php') ?>
