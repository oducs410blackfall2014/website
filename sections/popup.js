$(document).ready(function()
{
	var $team = null;

	$(document).on('click', '#popup-close', function(ev)
	{
		ev.preventDefault();
		ClosePopup();
	});
	$(document).on('click', '#popup-container', function(ev)
	{
		if ($(ev.target).find('#popup').length)
			ClosePopup();
	});
	$(document).on('keydown', function(ev) { if (ev.which == 27) ClosePopup(); });
	
	function LoadPopup(Function)
	{
		return $('<div>', { 'id': 'popup-container' }).load('sections/popup.php', Function);
	}
	function ShowPopup($Popup)
	{
		$Popup.hide().appendTo($('body')).fadeIn(200);
	}
	function ClosePopup()
	{
		$('#popup-container').fadeOut(200, function() { $(this).remove(); });
	}

	function PrepareBio($BioDiv)
	{
		var $popup = LoadPopup(function()
		{
			var $bio = $team[$BioDiv.attr('data-who')];
			ShowPopup($popup);
			console.log($);
			$('#popup-title').text($bio['name']);
			$('#popup-body').text($bio['bio']);
			$('#popup-image').attr('alt', $bio['name']);
			$('#popup-image').attr('src', $bio['image']);
		});
	}
	$('.bio').on('click', function(ev)
	{
		ev.preventDefault();
		var $this = $(this);
		
		//load team JSON
		if ($team == null)
		{
			$.getJSON('team.json', function(Data)
			{ 
				$team = Data;
				PrepareBio($this);
			});
		}
		else
			PrepareBio($this);
	});
});