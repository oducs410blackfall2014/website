<?php  require_once ('logic.php'); ?>
<!doctype html>
<html lang='en'>
<head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='icon' href='media/favicon.png'>
    <title>Sequoyah - CS 410 Black</title>
    <link href='http://fonts.googleapis.com/css?family=Roboto:300,400' rel='stylesheet' type='text/css'>
    <link rel='stylesheet' href='style.css'>
	<link href='http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css' rel='stylesheet'>
    <script type='text/javascript' src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js'></script>
	<script type="text/javascript" src="js/jquery.konami.min.js"></script>
    <script type='text/javascript'>$(window).konami({ cheat: function() { window.location="http://competitivenapping.wordpress.com/"; } });</script>
	<?php include_once ('sections/ga.php') ?>
</head>
<body>
<header>
	<div id='login'>
		<?php if (Auth\IsLoggedIn()): ?>
			<a href='logout.php'>Logout <i class='fa fa-sign-out'></i></a>
			<a href='admin.php'>Admin <i class='fa fa-gear'></i></a>
		<?php else: ?>
			<a href='login.php'>Login <i class='fa fa-sign-in'></i></a>
		<?php endif; ?>
	</div>
    <h1>
        <a id='logo' class='no-link' href='index.php' title='Sequoyah'>Sequoyah</a>
        <span class='hint middle'>
			&mdash; A CS 410 Project at 
			<a href='http://www.cs.odu.edu/'>ODU</a>
		</span>
	</h1>
    <nav>
        <a class='nav <?php echo @$page == 'home' ? 'active' : '' ?>' href='index.php' title='Home'>Home</a>
        <a class='nav <?php echo @$page == 'team' ? 'active' : '' ?>' href='team.php' title='The Team'>The Team</a>
        <a class='nav <?php echo @$page == 'probsol' ? 'active' : '' ?>' href='probsol.php' title='Problem/Solution Statements'>Problem / Solution</a>
        <a class='nav <?php echo @$page == 'research' ? 'active' : '' ?>' href='research.php' title='Research Progress'>Research Progress</a>
        <a class='nav <?php echo @$page == 'presentations' ? 'active' : '' ?>' href='presentations.php' title='Presentations'>Presentations</a>
		<a class='nav' href='https://bitbucket.org/oducs410blackfall2014' title='Bitbucket (External Link)'><i class='fa fa-bitbucket'></i> Bitbucket</a>
    </nav>
</header>