<div id='popup-bg'>
	<aside id='popup'>
		<h1 id='popup-title'></h1>
		<img id='popup-image' alt='' src=''>
		<div id='popup-body'></div>
		<div id='popup-footer'>
			<button id='popup-close'>Close</button>
		</aside>
	</div>
</div>