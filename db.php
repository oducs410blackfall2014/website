<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

class Db extends PDO
{
    const host = 'mysql.cs.odu.edu';
    const port = '3306';
    const dbName = '410black';
    const user = '410black';
    const pass = 'Monkeys bits are bonks.';

    public function __construct($options = null)
	{
        parent::__construct('mysql:host=' . Db::host . ';port=' . Db::port . ';dbname=' . Db::dbName, Db::user, Db::pass, $options);
    }

    public function query($query){ //secured query with prepare and execute
        $args = func_get_args();
        array_shift($args); //first element is not an argument but the query itself, should removed

        $reponse = parent::prepare($query);
        $reponse->execute($args);
        return $reponse;

    }

    public function insecureQuery($query){ //you can use the old query at your risk ;) and should use secure quote() function with it
        return parent::query($query);
    }

}

try
{
	$db = new Db();
	$db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
	return $db;
}
catch (Exception $expt)
{
	return null;
}