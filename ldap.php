<?php
    //simple LDAP connection wrapper -- Matt Hines
    class Ldap
    {
        private $server; //server url/name
        private $port; //port (usually 389)
        private $connectUser; //connected username

        private $ldap; //ldap connection
        private $bind; //ldap binding

        public function __construct($Server, $ConnectUsername, $ConnectPassword, $Port = 389)
        {
            $this->server = $Server;
            $this->port = $Port;
            $this->connectUser = $ConnectUsername;
            
            $this->ldap = @ldap_connect($Server, $Port);
            if ($this->ldap)
            {
                ldap_set_option($this->ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
                ldap_set_option($this->ldap, LDAP_OPT_REFERRALS, false);
                ldap_set_option($this->ldap, LDAP_OPT_DEBUG_LEVEL, 1);

                $this->bind = @ldap_bind($this->ldap, $ConnectUsername, $ConnectPassword);
                if (!$this->bind)
                {
                    $this->bind = null;
                    throw new Exception("Error binding to LDAP (Invalid Credentials)");
                }
            }
            else
            {
                $this->ldap = null;
                throw new Exception("Error connecting to LDAP");
            }
        }

        public function __destruct()
        {
            if (isset($this->ldap))
                ldap_close($this->ldap);   
        }

        public function GetLdap()
        {
            return $this->ldap;
        }

        public function IsGood($CheckForErrors = true)
        {
            return (isset($this->ldap) && (!$CheckForErrors || ldap_errno($this->ldap) == 0));
        }

        //return null if no error
        public function GetLastError()
        {
            return ldap_errno($this->ldap) ? ldap_error($this->ldap) : null;
        }

        //re-authenticate access user
        public function Rebind($connectUsername, $Connect_Password)
        {
            if ($this->ldap)
            {
                $this->bind = @ldap_bind($this->ldap, $connectUsername, $Connect_Password);
                if (!$this->bind)
                    throw new Exception("Error binding to LDAP");
            }
        }

        //Search the LDAP tree
        public function Search($BaseDN, $Filter, $Attributes = array(), $AttributesOnly = null, $SizeLimit = null, $TimeLimit = null, $Dereference = null)
        {
            $result = ldap_search($this->ldap, $BaseDN, $Filter, $Attributes, $AttributesOnly, $SizeLimit, $TimeLimit, $Dereference);

            if ($result === false || $result == null)
                return null;

            $info = ldap_get_entries($this->ldap, $result);
            ldap_free_result($result);
            return $info;
        }

    }
?>