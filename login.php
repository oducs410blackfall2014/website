<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

require_once ('logic.php');

if (Auth\IsLoggedIn())
{
	header('location: index.php');
	exit;
}

if (count(@$_POST) > 0)
{
	if (!Csrf\IsValid())
	{
		$_SESSION['login_last']['username'] = $_POST['username'];
		$_SESSION['login_last']['error'] = 'Invalid attempt';
		header('location: login.php');
	}
	
	if (Auth\Login($_POST['username'], $_POST['password']))
	{
		header('location: index.php');
		unset($_SESSION['login_last']);
		unset($_SESSION['CSRF']);
	}
	else
	{
		$_SESSION['login_last']['username'] = $_POST['username'];
		$_SESSION['login_last']['error'] = 'Invalid credentials';
		header('location: login.php');
	}
	
	exit;
}

Csrf\Create();

$page = 'login';
include ('sections/header.php');
?>
<form action='login.php' method='post'>
	<?php echo Csrf\GetFormPart(); ?>
	<h1>Login <span class='hint'>(Use your CS account)</span></h1><hr>
	
	<?php if (isset($_SESSION['login_last']['error'])): ?>
	<h2 class='form-error'><?php echo $_SESSION['login_last']['error']; ?></h2>
	<?php endif; ?>
	
	<label for='username'>Username</label>
	<input type='text' name='username' value='<?php echo @$_SESSION['login_last']['username']; ?>'>
	
	<label for='password'>Password</label>
	<input type='password' name='password' value=''>
	
	<input type='submit' value='Login'>
</form>
<?php unset($_SESSION['login_last']); include ('sections/footer.php'); ?>