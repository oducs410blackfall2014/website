<?php

namespace Auth
{
	require_once ('ldap.php');
	@session_start();

	function IsLoggedIn()
	{
		return isset($_SESSION['login']);
	}
	function GetUser()
	{
		return @$_SESSION['login'];
	}

	function Login($Username, $Password, $Group = 'CN=410black,OU=Unix Groups')
	{
		if (isset($_SESSION['login']))
			unset($_SESSION['login']);
		
		$domain = 'DC=cs,DC=odu,DC=edu';
		//auto-add the email to the account for logging in
		$email = $Username;
		$emailServer = '@cs.odu.edu';
		if (strpos($Username, $emailServer) === FALSE)
		{
			$email = $Username . $emailServer;
			$Username = str_replace($emailServer, '', $Username);
		}
		
		try
		{
			$ldap = new \Ldap('ad.cs.odu.edu', $email, $Password);
			$search = "(&(sAMAccountName=$Username)(memberOf=$Group,$domain))";
			$results = $ldap->Search($domain, $search, array('name')); //check if the user is in the group
			
			if ($results['count'] > 0)
			{
				$_SESSION['login'] = $Username;
				return true;
			}
		}
		catch (Exception $expt) { }
		return false;
	}

	function Logout()
	{
		unset($_SESSION['login']);
	}
}

//Cross-site request forgery protection
namespace Csrf
{
	function Generate()
	{
		return sha1(openssl_random_pseudo_bytes(32));	
	}
	function Create()
	{
		$csrf = Generate();
		$_SESSION['csrf'] = $csrf;
		return $csrf;
	}
	function Delete() { unset($_SESSION['csrf']); }

	function Created() { return isset($_SESSION['csrf']); }
	function Get() { return @$_SESSION['csrf']; }
	function GetFormPart() { return (Created() ? "<input type='hidden' id='csrf' name='csrf' value='{$_SESSION['csrf']}'>" : ""); }
	
	function IsValid() { return (isset($_SESSION['csrf']) && $_POST['csrf'] === $_SESSION['csrf']); }
}

namespace Files
{
	function IsValid($File)
	{
		return (isset($File) && !is_array(@$File['error']));
	}
	function Save($File, $Destination)
	{
		if (IsValid())
			return move_uploaded_file($File['tmp_name'], $Destination);
		return false;
	}
}

?>