<?php
$page = 'team';
include ('sections/header.php');

//error_reporting(E_ALL);
//ini_set('display_errors', 'on');

$team = json_decode(file_get_contents('team.json'));
?>	
<main>
	<div id='bios'>
	<?php foreach ($team as $user => $bio)
	{
		if (is_string($bio))
			echo $bio;
		else
		{
			echo "<a class='bio' data-who='{$user}'><div class='img-container'><img alt='{$bio->name}' src='{$bio->image}'></div>";
			echo $bio->name;
			if (!empty($bio->subtitle))
				echo "<div class='hint'>{$bio->subtitle}</div>";
			echo '</a>';
		}
	} ?>
	</div>
</main>
<script type='text/javascript' src='sections/popup.js'></script>
<?php include ('sections/footer.php') ?>
