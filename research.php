<?php $page='research'; include ('sections/header.php') ?>
<main>
    <h2 class='text-center'>Important Links</h2>
    <div class='text-center space-bottom'>
        <a class='link pad' href='http://www.nottowayindians.org/'>Official Website of the Nottoway Indian Tribe of Virginia</a><br>
        <a class='link pad' href='http://www.endangeredlanguages.com'>Endangered Languages Project</a><br>
        <a class='link pad' href='http://www.omniglot.com/writing/cherokee.htm'>Cherokee syllabary</a>
    </div>

    <h2 class='text-center'>Experts to Consult</h2>
    <dl class='space-bottom'>
        <dt>Typography Designer</dt>
        <dd>Can provide information on how fonts are constructed and elements involved.</dd>
        
        <dt>Linguist</dt>
        <dd>Can provide details on how language is constructed and understood. This information can help us make our tool more versatile for the problem domain.</dd>

        <dt>Unicode Consortium</dt>
        <dd>Can provide information on how new language characters are standardized for use on computer systems. We can also ensure our tool can generate character sets that can be standardized by the consortium if need be.</dd>
    </dl>

    <h2 class='text-center'>Customer Risks</h2>
    <div class='space-bottom'>
        <table class='risk-table'>
            <tr>
                <th></th>
                <th>Negligible</th>
                <th>Marginal</th>
                <th>Major</th>
                <th>Critical</th>
            </tr>
            <tr>
                <td class='ts'>Rare</td>
                <td class='risk good'>R3</td>
                <td class='risk good'>R1</td>
                <td class='risk good'></td>
                <td class='risk warning'></td>
            </tr>
            <tr>
                <td class='ts'>Unlikely</td>
                <td class='risk good'></td>
                <td class='risk good'></td>
                <td class='risk warning'></td>
                <td class='risk warning'></td>
            </tr>
            <tr>
                <td class='ts'>Possible</td>
                <td class='risk good'></td>
                <td class='risk warning'></td>
                <td class='risk warning'></td>
                <td class='risk bad'></td>
            </tr>
            <tr>
                <td class='ts'>Likely</td>
                <td class='risk warning'></td>
                <td class='risk warning'>R4</td>
                <td class='risk bad'></td>
                <td class='risk bad'></td>
            </tr>
            <tr>
                <td class='ts'>Certain</td>
                <td class='risk warning'></td>
                <td class='risk bad'>R2</td>
                <td class='risk bad'></td>
                <td class='risk bad'></td>
            </tr>
        </table>

        <ol class='risk-list'>
            <li>Customer computer illiteracy</li>
            <li>Customer simply not interested</li>
            <li>Lack of internet access for customer</li>
            <li>Lack of trust in product privacy/security</li>
        </ol>
    </div>

    <h2 class='text-center'>Technical Risks</h2>
    <div>
        <table class='risk-table'>
            <tr>
                <th></th>
                <th>Negligible</th>
                <th>Marginal</th>
                <th>Major</th>
                <th>Critical</th>
            </tr>
            <tr>
                <td class='ts'>Rare</td>
                <td class='risk good'>R3</td>
                <td class='risk good'></td>
                <td class='risk good'></td>
                <td class='risk warning'></td>
            </tr>
            <tr>
                <td class='ts'>Unlikely</td>
                <td class='risk good'></td>
                <td class='risk good'>R2</td>
                <td class='risk warning'></td>
                <td class='risk warning'></td>
            </tr>
            <tr>
                <td class='ts'>Possible</td>
                <td class='risk good'></td>
                <td class='risk warning'></td>
                <td class='risk warning'></td>
                <td class='risk bad'></td>
            </tr>
            <tr>
                <td class='ts'>Likely</td>
                <td class='risk warning'>R1</td>
                <td class='risk warning'></td>
                <td class='risk bad'>R4</td>
                <td class='risk bad'></td>
            </tr>
            <tr>
                <td class='ts'>Certain</td>
                <td class='risk warning'></td>
                <td class='risk bad'></td>
                <td class='risk bad'></td>
                <td class='risk bad'></td>
            </tr>
        </table>

        <ol class='risk-list'>
            <li>Problems generating a TrueType font for generated alphabet</li>
            <li>Maintaining the backend database and web server</li>
            <li>Maintaining accurate information in application</li>
            <li>Integrating with related language projects</li>
        </ol>
    </div>
</main>
<?php include ('sections/footer.php') ?>
