<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once ('logic.php');

if (!Auth\IsLoggedIn())
{
	header('location: index.php');
	exit;
}

$db = require_once('db.php');

Csrf\Create();

$page = 'admin';
include ('sections/header.php');
?>
<main>

	<h2 class='text-center space-bottom' id='stats'>
		<a class='link' target='_blank' href='https://www.google.com/analytics/web/?hl=en#report/visitors-overview/a55256714w88385376p91801025/'>Google Analytics</a>
		<div class='hint'>(Requires sign-in, use ODU email)</div>
	</h2>
</main>
<?php include ('sections/footer.php') ?>
